# Docker-OSM

## Configuração

Esse é um tutorial reduzido do encontrado no readme do [Kartoza](https://github.com/kartoza/docker-osm).

### 0. Docker
Para conseguir rodar a aplicação há a necessidade de ter o docker e o docker-compose, e o git, instalados na máquina.
```
sudo apt-get install docker
sudo apt-get install docker-compose
sudo apt-get install git
```

### 1. Download dos arquivos
Clone o repositório do [Kartoza](https://github.com/kartoza/docker-osm).

### 2. Download de arquivo .pbf
Entre no link http://download.geofabrik.de/ e baixe um continente ou país, para questão de teste baixei o da Antártica pois é o menor arquivo.

OBS: Atenção não baixar o .shp!

### 3. Construindo e subindo imagem
No terminal, dentro do diretório
```
docker-compose build
docker-compose up
```

# Créditos Docker OSM
Essa aplicação foi desenhada e implementada por:

* Etienne Trimaille (etienne.trimaille@gmail.com)
* Tim Sutton (tim@kartoza.com)

Como algumas importantes ideias propostas por Ariel Nunez (ingenieroariel@gmail.com).

Partes deste projeto são construídas sobre o trabalho existente de outras pessoas.

Julho 2015
