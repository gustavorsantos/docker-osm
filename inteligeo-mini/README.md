Inteligeo Mini
==============

Introdução
-------------

O Inteligeo Mini é uma versão simplificada da ferramenta [Inteligeo]. É um componente com o objetivo de facilitar o georreferenciamento de dados e a utilização de dados do Inteligeo no portal de outros sistemas. Sua integração é simples, como explicado a seguir.

## Requisitos
Para rodar o Inteligeo Mini, as seguintes bibliotecas são requeridas:
 * [Leaflet] - Para gerar o mapa
 * [Leaflet WMS] - Para carregar as camadas de sobreposição (*overlays*)
 * [Exif] - Para ler os metadados de uma imagem

## Funcionalidades

 * Geração da coordenada em graus decimais a partir de um clique no mapa
 * Exibição de marcador no mapa a partir do envio de coordenadas
 * Leitura de coordenadas geográficas de arquivos .jpg arrastados para a janela do mapa (*drag and drop*)
 * Exclusão do marcador e coordenadas

## Funcionalidades auxiliares
 * Conversão entre coordenadas, de/para latlong e as notações seguintes:
    * Graus decimais: GGG.GGGGGGGGº N/S/W/E
    * Graus, minutos decimais: GGGºMM.MMMMM' N/S/W/E
    * Graus, minutos, segundos: GGGºMM'SS.SSS" N/S/W/E
    * UTM: FF N/S XXXXXX.X YYYYYY.Y
 * Conversão de datum entre:
    * WGS 84 ou SIRGAS 2000
    * SAD 69
    * Córrego Alegre
    * Astro Chuá

Para utilizar a conversão entre notações e datum, é necessário importar o arquivo `convert.coordinates.js`.

## Modo de uso

### Camada base (TileLayer)

#### Adicionar camada base
Para adicionar uma camada base (TileLayer), é necessário criar um evento de clique em um botão (ou o que achar melhor) e chamar a função `addBasemap` passando o `título` que aparecerá no controlador, o `id` único, a `URL` e a `atribuição` da camada. Duas camadas não podem ser criadas com o mesmo `id`.

```javascript
// Gerencia clique no botão Submit para inserir novo Basemap
addBasemap = function (e) {
    e.preventDefault();

    let BasemapTitle = document.getElementById('BasemapTitle').value;
    let BasemapID = document.getElementById('BasemapID').value;
    let BasemapURL = document.getElementById('BasemapURL').value;
    let BasemapAttr = document.getElementById('BasemapAttr').value;

    mini.addBasemap(BasemapTitle,BasemapID, BasemapURL, BasemapAttr);
}
var submit = document.getElementById('submitBasemap');
submit.addEventListener('click', addBasemap)
```

#### Remover camada base
Para remover uma camada, é utilizada a função `removeBasemapByID` passando o `id` único a ser removido.

```javascript
// Gerencia clique no botão Delete para remover Basemap através de seu id
removeBasemapByID = function (e) {
    e.preventDefault();

    let BasemapID = document.getElementById('removeBasemapByID').value;

    mini.removeBasemapByID(BasemapID);
}
var submit = document.getElementById('btnRemoveBasemapByID');
submit.addEventListener('click', removeBasemapByID);
```

### Camadas de sobreposição

#### Adicionar camada de sobreposição
Uma camada de sobreposição é adicionada ao mapa através da função `addOverlay` com os parâmetros `overlayID` (id único), `overlayURL` (URL), `overlayLayers` (conjunto de subcamadas que compõem a camada), `overlayFormat` (formato), `overlayAttr` (atribuição).

```javascript
// Gerencia o clique no botão Submit para adicionar uma camada de sobreposição
addOverlay = function (e) {

    e.preventDefault();

    let overlayID = document.getElementById('OverlayName').value;
    let overlayURL = document.getElementById('OverlayURL').value;
    let overlayLayers = document.getElementById('OverlayLayers').value;
    let overlayFormat = document.getElementById('OverlayFormat').value;
    let overlayAttr = document.getElementById('OverlayAttr').value;

    mini.addOverlay(overlayID, overlayURL, overlayLayers, overlayFormat, overlayAttr);

}
var submit = document.getElementById('submitOverlay');
submit.addEventListener('click', addOverlay);
```

#### Remover camada de sobreposição
Para remover uma camada de sobreposição, utiliza-se a função `removeOverlayByID` passando o `id` único da camada.

```javascript
removeOverlayByID = function(e) {
    e.preventDefault();

    let overlayName = document.getElementById('overlayToRemove').value;

    mini.removeOverlayByID(overlayName);
}
var submit = document.getElementById('removeOverlayByID');
submit.addEventListener('click', removeOverlayByID);
```

### Manipulação do mapa

#### Definir zoom
Utiliza-se a função `setZoom` passando um valor inteiro referente ao nível de zoom para ser definido.
```javascript
setZoom = function(e) {
    e.preventDefault();
            
    var zoom = document.getElementById('setZoom').value;
        
    mini.setZoom(zoom);
}
var submit = document.getElementById('submitZoom');
submit.addEventListener('click', setZoom);
```

### Centralizar em um ponto
Utiliza-se a função `panTo` para centralizar o mapa em um ponto passando a `latitude` e a `longitude` dele.
```javascript
panTo = function(e) {
    e.preventDefault()

    var lat = document.getElementById('setLat').value;
    var lng = document.getElementById('setLng').value;
            
    mini.panTo(lat, lng);
}
```

### Centralizar em uma área
A função `fitBounds` centraliza o mapa em uma área com o maior nível possível de zoom. São passadas a latitude e a longitude de um ponto A e de um ponto B.

```javascript
fitBounds = function(e) {
    e.preventDefault();

    var latMin = document.getElementById('latMin').value;
    var lngMin = document.getElementById('lngMin').value;
    var latMax = document.getElementById('latMax').value;
    var lngMax = document.getElementById('lngMax').value;

    mini.fitBounds(latMin, lngMin, latMax, lngMax);
}
var submit = document.getElementById('submitBounds');
submit.addEventListener('click', fitBounds);
```

### HTML e CSS
Para que o mapa seja criado na página, é necessário que se declare uma `div` que será utilizada como conteiner para o mapa, com um `id`, que será utilizado para instanciar o mapa.
```html
<div id="map"></div>
```
É necessário ainda que essa `div` tenha seu tamanho pré-definido, podendo ser definido no HTML...
```html
<div id="map" style="width:600px; height=400px;"></div>
```
Ou no CSS...
```css
#map {
    height: 400px;
    width: 600px
}
```
> **Nota:** Não há um nome pré-definido para o `id` atribuído à `div`, mas ele deve ser o mesmo a ser utilizado na instanciação do objeto da classe Mini.

### JavaScript
#### Instanciando o mapa
Para instanciar a classe, é necessário passar dois atributos como parâmetros: O `id` definido no HTML anterior e um objeto com opções para criação do mapa (mais opções em [Leaflet API Reference])
```javascript
var mini = new Mini('map', {
    center: [-12, -51.1],
    zoom: 4,
    minZoom: 2,
});
```
#### Implementando a funcionalidade de clique no mapa
Para gerenciar o evento de clique no mapa, deve-se chamar a função `onMapClick` a partir do objeto criado, passando como parâmetro uma função de callback, que receberá as coordenadas geradas no mapa. Dessa forma, é possível fazer uma validação ou qualquer outro tipo de tratamento para os dados recebidos. Um exemplo básico é fazer a atribuição das coordenadas em campos de `input`.
```javascript
updateInputFields = function (x, y) {
    document.getElementById('lng').value = x
    document.getElementById('lat').value = y
}
mini.onMapClick(updateInputFields);
```
Para utilizar a conversão de latlong para outras notações/datum, deve-se chamar a função `fromLngLat (lng, lat, notation, datum)` do helper `convert.coordinates.js`. Um exemplo de utilização pode ser visto abaixo:
```javascript
updateInputFields = function (x, y) {
    // função para converter as coordenadas de latlng para UTM em WGS 84
    let coordinates = fromLngLat(x, y, 'utm', 'wgs84')
    
    document.getElementById('lng').value = coordinates.lng;
    document.getElementById('lat').value = coordinates.lat;
    document.getElementById('fuso').value = coordinates.fuso[0];
    document.getElementById('hem').value = coordinates.fuso[1];

    // função para converter as coordenadas de latlng para Grau, Minuto e Segundo em SAD 69
    coordinates = fromLngLat(x, y, 'dms', 'sad69')

    document.getElementById('grauY').value = coordinates.lat[0];
    document.getElementById('minY').value = coordinates.lat[1];
    document.getElementById('segY').value = coordinates.lat[2];
    document.getElementById('hemY').value = coordinates.lat[3];
    document.getElementById('grauX').value = coordinates.lng[0];
    document.getElementById('minX').value = coordinates.lng[1];
    document.getElementById('segX').value = coordinates.lng[2];
    document.getElementById('hemX').value = coordinates.lng[3];

}
mini.onMapClick(updateInputFields);
```
Valores aceitos para notação:
 * **'d'**: Graus decimais: GGG.GGGGGGGGº N/S/W/E
 * **'dm'**: Graus, minutos decimais: GGGºMM.MMMMM' N/S/W/E
 * **'dms'**: Graus, minutos, segundos: GGGºMM'SS.SSS" N/S/W/E
 * **'utm'**: UTM: FF N/S XXXXXX.X YYYYYY.Y

 Valores aceitos para datum:
  * **'wgs84'**: WGS 84
  * **'sirgas2000'**: SIRGAS 2000
  * **'sad69'**: SAD 69
  * **'corregoale'**: Córrego Alegre
  * **'astrochua'**: Astro Chuá

#### Implementando a funcionalidade de envio de coordenadas para o mapa
Para gerenciar o envio das coordenadas para o mapa, é necessário criar um evento de clique em algum botão (ou outra forma que julgar mais pertinente), e chamar a função `centerMap` passando as coordenadas como argumentos. Isso é útil para quando o usuário já possui as coordenadas e quer vê-las renderizadas no mapa. Um exemplo básico é visto abaixo, com o valor das coordenadas em decimais sendo obtidos a partir de campos do tipo `input`.
```javascript
sendToMap = function (e) {
    e.preventDefault();
    
    let x = document.getElementById('lng').value
    let y = document.getElementById('lat').value
    mini.centerMap(x, y)
}
var submit = document.getElementById('submit');
submit.addEventListener('click', sendToMap);
```
É possível, a partir de outras notações e projeções, converter o dado para decimal, chamando a função `toLngLat(coordinates, notation, datum)` do helper `convert.coordinates.js`, para então chamar a função `centerMap`, da seguinte forma.
```javascript
// Gerencia clique no botão submit para coordenadas UTM
sendToMap = function (e) {
    e.preventDefault();

    let x = document.getElementById('lng').value;
    let y = document.getElementById('lat').value;
    let f = document.getElementById('fuso').value;
    let h = document.getElementById('hem').value;
    let coordinates = {};
    coordinates.lat = [y];
    coordinates.lng = [x];
    coordinates.fuso = [f, h];
    
    [x, y] = toLngLat(coordinates, 'utm', 'sad69');
    mini.centerMap(x, y);
}
var submit = document.getElementById('submitUTM');
submit.addEventListener('click', sendToMap);
```
> **Nota:** A variável `coordinates` precisa ser um objeto que contém três atributos em formato de array - lat, lng e fuso (para o caso de UTM). Os atributos lat e lng devem ser da forma:
> * **'d'**: [GGG]
> * **'dm'**: [GGG, MM.MMMMM]
> * **'dms'**: [GGG, MM, SS.SSS, 'N/S/W/E']
> * **'utm'**: [XXXXXX.X]
>
> Para a notação UTM, o campo fuso tem o formato:
> * **utm**: [FF, 'N/S']

#### Implementando a funcionalidade de obtenção das coordenadas a partir de uma imagem georreferenciada
Para gerenciar a obtenção das coordenadas a partir de uma imagem, deve-se chamar a função `onDropImage` a partir do objeto criado, passando como parâmetro uma função de callback, que receberá um objeto contendo quatro atributos:
1. `status`: contém o status da requisição - **0** significa que a requisição foi bem sucedida. Qualquer outro valor, indica um erro.
2. `message`: contém a mensagem correspondente ao status.
3. `x`: contém o valor correspondente à longitude, em caso de sucesso.
4. `y`: contém o valor correspondente à latitude, em caso de sucesso.

Um exemplo de tratamento para o *drag and drop* de uma imagem no mapa é mostrado a seguir:
```javascript
imageResponse = function (imgResponse) {
    if (imgResponse.status !== 0) {
        alert(imgResponse.message)
    }
    else {
        document.getElementById('lng').value = imgResponse.x
        document.getElementById('lat').value = imgResponse.y
    }
}
mini.onDropImage(imageResponse)
```
#### Implementando a funcionalidade de exclusão do marcador
Para excluir um marcador e suas respectivas coordenadas geradas, é necessário criar um evento de clique em algum botão (ou outra forma que julgar mais pertinente), e chamar a função `deleteMarker`. Um exemplo básico é visto abaixo.
```javascript
deleteMarker = function (e) {
    e.preventDefault();

    document.getElementById('lng').value = ''
    document.getElementById('lat').value = ''
    mini.deleteMarker();
}
var deleteButton = document.getElementById('delete');
deleteButton.addEventListener('click', deleteMarker);
```

#### Habilitando o zoom a partir da combinação Shift + Scroll do mouse
De modo a que a funcionalidade de rolagem da página com o scroll do mouse não interfira diretamente no mapa e vice-versa, tendo um comportamento inesperado, a biblioteca do Inteligeo Mini possui a função `bindScrollWheelZoomtoShift`. Ela desabilita o zoom direto com o Scroll, quando se está com o mouse sobre o mapa, e habilita o zoom apenas a partir do scroll com a tecla Shift pressionada.

Sua utilização pode ser feita da seguinte forma:
```javascript
// Permite a utilização do Zoom com a combinação de Shift + Scroll
mini.bindScrollWheelZoomtoShift();
```
Para a funcionalidade rodar adequadamente, é necessário adicionar o seguinte CSS na página:
```css
.map-scroll:before {
    content: 'Use Shift + Scroll do mouse para dar zoom no mapa';
    position: absolute;
    top: 40%;
    left: 10%;
    z-index: 999;
    font-size: 26px;
}

.map-scroll:after {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
    content: '';
    background: #00000061;
    z-index: 999;
}
```

As funções a seguir estão disponíveis:
| Nome | Descrição |
| ---: | :-------- |
| `onMapClick(callback)` | Cria o marcador após um clique no mapa |
| `centerMap(x, y)` | Cria o marcador no mapa após o envio de um par de coordenadas |
| `onDropImage(callback)` | Cria o marcador no mapa após o arraste e  |soltura | de uma imagem com coordenadas |
| `deleteMarker()` | Exclui o marcador do mapa |
| `bindScrollWheelZoomtoShift()` | Habilita zoom a partir de Shift + Scroll |


## Exemplos

Cada um dos exemplos está documentado com o seu próprio arquivo Readme.

O [Exemplo 01](exemplo01/README.md) demonstra o armazenamento local das bibliotecas javascript utilizadas. Essa abordagem é potencialmente
mais rápida, evita requisições adicionais de DNS e pode funcionar inteiramente na rede interna de uma instituição, sem exigir
uma conexão de internet.

O [Exemplo 02](exemplo02/README.md) demonstra a utilização de CDN para baixar as bibliotecas da internet. O código fica mais simples e um pouco
mais fácil de manter.


[Leaflet]: https://unpkg.com/leaflet@1.4.0/dist/leaflet.js "Leaflet Js"
[Leaflet WMS]: https://cdn.rawgit.com/heigeo/leaflet.wms/gh-pages/dist/leaflet.wms.js "Leaflet WMS Js"
[Exif]: https://cdn.jsdelivr.net/npm/exif-js "EXIF Js"
[Inteligeo]: https://www.inteligeo.ditec.pf.gov.br/portal/ "Portal Inteligeo"
[Leaflet API Reference]: https://leafletjs.com/reference-1.4.0.html#map-option "Leaflet Reference #map-option"
