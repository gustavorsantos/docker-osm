/**
 * @file Classe do Inteligeo Mini
 * @author Eder Carlos
 * @version 1.0.0 
 * @copyright (c) AGEO/INC/DITEC/DPF 2019
 */
const SUCCESS = 0;
const ERROR_NO_FILE = 1;
const ERROR_INVALID_FORMAT = 2;
const ERROR_NO_COORDINATES = 3;

/**
 * @class Inteligeo Mini
 */
var Mini = (function (L) { /**
     * Instancia um mapa utilizando o Leaflet.js
     * @constructor
     * @param {string} id - O id da div em que o mapa será instanciado.
     * @param {Object} options - Opções de instância do mapa.
     */
    function Mini(id, options) {
        this._map = L.map(id, options);
        this._map.setMaxBounds([
            [
                84.67351256610522, -174.0234375
            ],
            [
                -58.995311187950925, 223.2421875
            ]
        ]);

        this.basemaps = {};
        this.overlays = {};

    }

    /**
     * Adiciona uma camada base (TileLayer) no mapa, com parâmetros definidos
     * pelo usuário.
     * @param {string} BasemapTitle // Nome da camada que aparecerá no controlador
     * @param {string} BasemapID // Identificador da camada
     * @param {string} BasemapURL // URL da camada
     * @param {string} BasemapAttr // String para controle de atributos
     * de atributos
     */
    Mini.prototype.addBasemap = function (BasemapTitle, BasemapID, BasemapURL, BasemapAttr) { // checa se já existe uma camada com o mesmo identificador
        if (this.basemaps[BasemapID]) 
            return;
        


        this.basemaps[BasemapID] = {}

        // armazenar titulo para poder criar o link para o inteligeo
        this.basemaps[BasemapID]['title'] = BasemapTitle;
        this.basemaps[BasemapID]['url'] = BasemapURL;
        this.basemaps[BasemapID]['attribution'] = BasemapAttr;
        this.basemaps[BasemapID]['layerRef'] = L.tileLayer(BasemapURL, {
            attribution: BasemapAttr,
            noWrap: true
        });

        // cria um controlador se ele já não existir
        if (!this.layerControl) 
            this.layerControl = L.control.layers(null, null).addTo(this._map);
        


        this.layerControl.addBaseLayer(this.basemaps[BasemapID]['layerRef'], this.basemaps[BasemapID]['title']);

    }

    Mini.prototype.getBasemapName = function () {

        for (var key in this.basemaps) {
            console.log(keys)
            console.log(this.basemaps[key]['name']);
        }

    }

    /**
     * Remove a camada base do mapa
     * @param {string} BasemapID // Nome da camada para remover
     */
    Mini.prototype.removeBasemapByID = function (BasemapID) { // remove a camada do controlador

        this.layerControl.removeLayer(this.basemaps[BasemapID]['layerRef']);

        // remove a camada do mapa
        this._map.removeLayer(this.basemaps[BasemapID]['layerRef']);

        // remove a camada do objeto basemaps
        delete this.basemaps[BasemapID];

    }

    Mini.prototype.addOverlay = function (overlayID, overlayLURL, overlayLayers, overlayFormat, overlayAttr) {

        this.overlays[overlayID] = L.tileLayer.wms(overlayLURL, {
            layers: overlayLayers,
            format: overlayFormat,
            transparent: true,
            noWrap: true,
            srs: 'EPSG:4326'
        });

        // cria um controlador se ele já não existir
        if (!this.layerControl) 
            this.layerControl = L.control.layers(null, null).addTo(this._map);
        


        this.layerControl.addOverlay(this.overlays[overlayID], overlayAttr);

    }

    Mini.prototype.removeOverlayByID = function (overlayID) {

        this.layerControl.removeLayer(this.overlays[overlayID]);

        // remove a camada do mapa
        this._map.removeLayer(this.overlays[overlayID]);

        // remove a camada do objeto basemaps
        delete this.overlays[overlayID];
    }

    Mini.prototype.linkToInteligeo = function (lat, lng, zoom, basemap) { // converter 6 casas decimais
        lat = Number(Math.round(parseFloat(lat + 'e' + 6)) + 'e-' + 6);
        lng = Number(Math.round(parseFloat(lng + 'e' + 6)) + 'e-' + 6);

        var link = "https://www.inteligeo.ditec.pf.gov.br/portal/?lat=" + lat + "&lon=" + lng + "&z=" + zoom;
        link += "&basemap=" + basemap;

        console.log(link)
    }

    /**
     * Habilita o zoom a partir do scroll do mouse apenas com a tecla Shift pressionada.
     * @param {MouseEvent} e - Evento de scroll do mouse wheel.
     */
    Mini.prototype._enableShiftScroll = function (e) {
        e.stopPropagation();
        if (e.shiftKey == true) {
            e.preventDefault();
            this._map.scrollWheelZoom.enable();
            this._removeMapScrollClass();
            (function () {
                setTimeout(function () {
                    this._map.scrollWheelZoom.disable();
                }, 1000);
            }).bind(this);
        } else {
            this._map.scrollWheelZoom.disable();
            this._addMapScrollClass();
        }
    };

    /**
     * Adiciona a classe 'map-scroll' no mapa para exibir informação 
     * sobre utilização do scroll do mouse.
     */
    Mini.prototype._addMapScrollClass = function () {
        return this._map._container.classList.add('map-scroll');
    }

    /**
     * Remove a classe 'map-scroll' no mapa para exibir informação 
     * sobre utilização do scroll do mouse.
     */
    Mini.prototype._removeMapScrollClass = function () {
        return this._map._container.classList.remove('map-scroll');
    }

    /**
     * Faz todos os binds para o caso de utilização da combinação Shift + Scroll do mouse.
     */
    Mini.prototype.bindScrollWheelZoomtoShift = function () {
        this._map.scrollWheelZoom.disable();
        L.DomEvent.on(this._map._container, 'mousewheel DOMMouseScroll', this._enableShiftScroll.bind(this));
        L.DomEvent.on(this._map.zoomControl._container.firstChild, 'click', this._removeMapScrollClass.bind(this));
        L.DomEvent.on(this._map.zoomControl._container.lastChild, 'click', this._removeMapScrollClass.bind(this));
        this._map.on('movestart', this._removeMapScrollClass.bind(this));
    };

    /**
     * Esse tipo de callback é chamado `createMarkerCallback` e é mostrado como parte da classe Mini
     *
     * @callback createMarkerCallback
     * @param {number} x - Longitude em decimal
     * @param {number} y - Latitude em decimal.
     * @memberof Mini
     */

    /**
     * Cria um marcador (marker) no mapa a partir de um evento
     * @callback
     * @param {Mini~createMarkerCallback} callback - A função de callback que retornará o par de coordenadas (lat, lng).
     * @param {MouseEvent} e - Evento de clique do mouse, que contém o par de coordenadas (lat, lng).
     */
    Mini.prototype._createMarker = function (callback, e) {
        this._removeMapScrollClass();
        if (this._marker) {
            this._map.removeLayer(this._marker);
        }
        this._marker = new L.Marker(e.latlng).addTo(this._map);

        try {
            if (typeof callback === "function") {
                if (callback.length === 2) {
                    callback(e.latlng.lng, e.latlng.lat);
                } else {
                    throw new Error('Quantidade de argumentos da função de callback incorreto. Número de argumentos esperado: 2')
                }
            } else {
                throw new Error('Esperado uma função de callback. Recebido um argumento do tipo \'' + typeof callback + '\'')
            }
        } catch (e) {
            console.log(e)
        }
    }

    /**
     * Cria um marcador (marker) no mapa a partir de um evento
     * @callback
     * @param {Mini~createMarkerCallback} callback - A função de callback que retornará o par de coordenadas (lat, lng).
     */
    Mini.prototype.onMapClick = function (callback) {
        this._map.on('click', this._createMarker.bind(this, callback))
    }

    /**
     * Cria um marcador (marker) a partir de um par de coordenadas (lat, lng).
     * @param {number} lng - Longitude em decimal.
     * @param {number} lat - Latitude em decimal.
     */
    Mini.prototype.centerMap = function (lng, lat) {
        this._removeMapScrollClass();
        lng = this._tolerantParseFloat(lng);
        lat = this._tolerantParseFloat(lat);
        if (this._marker) {
            this._map.removeLayer(this._marker);
        }
        this._marker = new L.Marker([lat, lng]).addTo(this._map);
        this._map.setView([lat, lng])
    }

    /**
     * @typedef {Object} ImageResponse
     * @property {number} status - O identificador do status
     * @property {string} message - A mensagem correspondente ao status.
     * @property {number} lng - Longitude em decimal.
     * @property {number} lat - Latitude em decimal.
     * @memberof Mini
     */

    /**
     * Obtém o status de carregamento da imagem
     * @param {number} status - O identificador do status.
     * @param {File} file - O arquivo carregado.
     * @returns {Mini~ImageResponse} Objeto contendo status de retorno do carregamento da imagem
     */
    Mini.prototype._getImageDropStatus = function (status, file) {
        file = file || null;
        let name = (file) ? file.name : ''
        let type = (file) ? file.type : ''
        let statusMsg = {}
        statusMsg[SUCCESS] = 'Coordenadas da imagem obtidas com sucesso'
        statusMsg[ERROR_NO_FILE] = 'Arquivo inválido. Envie somente arquivos JPEG.'
        statusMsg[ERROR_INVALID_FORMAT] = 'Arquivo \'' + name + '\' no formato \'' + type + '\' inválido. Envie somente arquivos JPEG.'
        statusMsg[ERROR_NO_COORDINATES] = 'A imagem \'' + name + '\' não tem coordenadas.'

        let imgResponse = {
            status: status,
            message: statusMsg[status]
        }

        return imgResponse
    }

    /**
     * Obtém os metadados de uma imagem
     * @param {File} imageFile - O arquivo de imagem obtido através do evento de drop.
     * @returns {Promise<Object>} Uma Promise que retorna os metadados da imagem
     */
    Mini.prototype._getImageMetaData = function (imageFile) {
        return new Promise(function (resolve, reject) {
            try {
                EXIF.getData(imageFile, function () {
                    resolve(EXIF.getAllTags(this))
                })
            } catch (e) {
                reject(e)
            }
        })
    }

    /**
     * Cria um marcador (marker) no mapa a partir do evento de drop de uma imagem com coordenadas geográficas
     * @callback
     * @param {createMarkerCallback} callback - A função de callback que retornará o par de coordenadas (lat, lng).
     * @param {DropEvent} e - Evento de drop, que contém o arquivo carregado
     * @returns {Promise<ImageResponse>} Uma Promise que retorna um objeto ImageResponse com o par de
     *          coordenadas (lat, lng) em caso de sucesso.
     */
    Mini.prototype._createMarkerFromImageCoordinates = function (callback, e) {
        e.preventDefault()
        let files = e.dataTransfer.files;
        let promiseFile;
        if (files.length < 1) {
            promiseFile = Promise.reject(this._getImageDropStatus(ERROR_NO_FILE))
        } else {
            let file = files[0]
            if (file.type !== 'image/jpeg') {
                promiseFile = Promise.reject(this._getImageDropStatus(ERROR_INVALID_FORMAT, file))
            } else {
                promiseFile = this._getImageMetaData(file).then(function (allTags) {
                    let lat = allTags.GPSLatitude
                    let lng = allTags.GPSLongitude
                    if (lat === undefined || lng === undefined) {
                        return Promise.reject(this._getImageDropStatus(ERROR_NO_COORDINATES, file))
                    } else {
                        let latRef = allTags.GPSLatitudeRef || 'N'
                        let lngRef = allTags.GPSLongitudeRef || 'W'
                        lat = (lat[0] + lat[1] / 60 + lat[2] / 3600) * (latRef === 'N' ? 1 : -1)
                        lng = (lng[0] + lng[1] / 60 + lng[2] / 3600) * (lngRef === 'W' ? -1 : 1)
                        let imgResponse = this._getImageDropStatus(SUCCESS)
                        imgResponse.y = lat
                        imgResponse.x = lng

                        return imgResponse;
                    }
                }.bind(this), function (err) {
                    return Promise.reject(callback(err))
                })
            }
        }
        Promise.resolve(promiseFile).then(function (res) {
            this.centerMap(res.x, res.y)
            callback(res)
        }.bind(this), function (err) {
            callback(err)
        })
    }

    /**
     * Cria o evento de dragOver no mapa para arrastar imagem
     */
    Mini.prototype._activateDragOver = function () {
        L.DomEvent.on(this._map._container, 'dragover', function (e) {
            e.preventDefault()
            e.dataTransfer.dropEffect = "move"
        })
    }

    /**
     * Cria o evento de drop no mapa para soltar imagem
     * @callback
     * @param {createMarkerCallback} callback - A função de callback que retornará o par de coordenadas (lat, lng).
     */
    Mini.prototype.onDropImage = function (callback) {
        this._activateDragOver();

        L.DomEvent.on(this._map._container, 'drop', this._createMarkerFromImageCoordinates.bind(this, callback))
    }

    /**
     * Exclui um marcador (marker) do mapa
     */
    Mini.prototype.deleteMarker = function () {
        if (this._marker) {
            this._map.removeLayer(this._marker)
        }
    }

    /**
      Converte um valor para float, caso ocorrer uma falha retorna 0.
      @param {*} num - O valor a ser convertido.
    */
    Mini.prototype._tolerantParseFloat = function (num) {
        if (typeof num === 'string') 
            num = num.replace(',', '.')


        


        let ret = parseFloat(num)
        return(isNaN(ret)) ? 0 : ret
    }

    /**
     * Define o zoom do mapa
     * @param {*} zoom - O nível de zoom a ser definido
     */
    Mini.prototype.setZoom = function (zoom) {

        this._map.setZoom(zoom);

    }

    /**
     * Mover o mapa para as coordenadas desejadas
     * @param {array?} latlng - Coordenadas para onde mover o mapa
     */
    Mini.prototype.panTo = function (lat, lng) {

        this._map.panTo([lat, lng]);

    }

    /**
     * Centralizar em uma área que apareça com o máximo de zoom possível
     * @param latMin - latitude do primeiro canto
     * @param lngMin - longitude do primeiro canto
     * @param latMax - latitude do segundo canto
     * @param lngMax - longitude do segundo canto
     */
    Mini.prototype.fitBounds = function (latMin, lngMin, latMax, lngMax) {

        this._map.fitBounds([
            [
                latMin, lngMin
            ],
            [
                latMax, lngMax
            ],

        ]);

    }

    Mini.prototype.renderGeoJSON = function (geojsonFeature, options) {

        // insere css para remover o fundo do tooltip
        var sheet = window.document.styleSheets[0];
        sheet.insertRule('.geojson-tooltip-label-style { \
                                background-color: transparent; \
                                border: transparent; \
                                box-shadow: none; \
                                font-weight: bold; \
                            }', sheet.cssRules.length);


        if (options) {

            function onEachFeature(feature, layer) {

                if (options.labelKey && feature.properties[options.labelKey]) {

                    layer.bindTooltip(feature.properties[options.labelKey], {
                        permanent: true,
                        direction: 'center',
                        className: 'geojson-tooltip-label-style'
                    });

                }

                if (options.popup && feature.properties) { 
                    
                    // Transforma as properties para html no popup
                    var popupContent = "<table>";

                    for (var key in feature.properties) {

                        if (feature.properties.hasOwnProperty(key)) {

                            popupContent += `<tr><th>${key}</th><td>${
                                feature.properties[key]
                            }</td></tr>`;

                        }

                    }

                    popupContent += "</table>"

                    layer.bindPopup(popupContent);

                }

            }


        }

        // cria o GeoJSON e adiciona ao mapa
        var geojsonLayer = L.geoJSON(geojsonFeature, {onEachFeature: onEachFeature}).addTo(this._map);

        if (options.title) {

            if (!this.layerControl) {

                this.layerControl = L.control.layers(null, null).addTo(this._map);

            }

            this.layerControl.addOverlay(geojsonLayer, options.title);

        }


    }


    return Mini;

})(L);
