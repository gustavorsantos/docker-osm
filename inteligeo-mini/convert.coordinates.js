(function () {
    const datumProjection = {
        wgs84: "+datum=WGS84 +no_defs",
        sirgas2000: "+datum=WGS84 +no_defs",
        sad69: "+ellps=aust_SA +towgs84=-67.35,3.88,-38.22",
        corregoalegre: "+ellps=intl +towgs84=-205.57,168.77,-4.12",
        astrochua: "+ellps=intl +towgs84=-144.35,242.88,-33.22"
    }

    /**
      Converte um valor para float, caso ocorrer uma falha retorna 0.
      @param {*} num - O valor a ser convertido.
    */
    _tolerantParseFloat = function (num) {
        if (typeof num === 'string') num = num.replace(',', '.')
        let ret = parseFloat(num)
        return (isNaN(ret)) ? 0 : ret
    }

    /**
     * Converte um valor para inteiro, caso ocorrer uma falha retorna 0.
     * @param {*} num - O valor a ser convertido.
     */
    _tolerantParseInt = function (num) {
        let ret = parseInt(num)
        return (isNaN(ret)) ? 0 : ret
    }

    /**
     * Converte as coordenadas de latlong para qualquer notação e datum disponíveis.
     * @param {number} lng - Longitude.
     * @param {number} lat - Latitude.
     * @param {String} notation - String que indica a notação a ser convertida.
     * @param {String} datum - String contendo o nome do datum a ser convertido.
     */
    fromLngLat = function(lng, lat, notation, datum) {
        let x, y, utmString
        [x, y] = proj4('EPSG:4326', datumProjection[datum] + ' +proj=longlat', [lng, lat])
        let hx = (x >= 0) ? 'E' : 'W'
        let hy = (y >= 0) ? 'N' : 'S'
        let z = Math.ceil((x + 180) / 6)
        
        let xABS = Math.abs(x)
        let yABS = Math.abs(y)
        switch (notation) {
            case 'd': // Graus decimais: GGG.GGGGGGGGº N/S/W/E
                return {
                    lat: [Math.round(yABS * 10000000) / 10000000, hy],
                    lng: [Math.round(xABS * 10000000) / 10000000, hx]
                }
            case 'dm': // Graus, minutos decimais: GGGºMM.MMMMM' N/S/W/E
                return {
                    lat: [Math.floor(yABS), Math.round((yABS % 1) * 60 * 100000) / 100000, hy],
                    lng: [Math.floor(xABS), Math.round((xABS % 1) * 60 * 100000) / 100000, hx]
                }
            case 'dms': // Graus, minutos, segundos: GGGºMM'SS.SSS" N/S/W/E
                return {
                    lat: [Math.floor(yABS), Math.floor((yABS % 1) * 60), Math.round(((yABS * 60) % 1) * 60 * 1000) / 1000, hy],
                    lng: [Math.floor(xABS), Math.floor((xABS % 1) * 60), Math.round(((xABS * 60) % 1) * 60 * 1000) / 1000, hx]
                }
            case 'utm': // UTM: FF N/S XXXXXX.X YYYYYY.Y
                utmString = datumProjection[datum] + ' +proj=utm +zone=' + z + ((hy === 'N') ? ' +north' : ' +south');
                [x, y] = proj4('EPSG:4326', utmString, [lng, lat])
                return {
                    lat: Math.round(y * 10) / 10,
                    lng: Math.round(x * 10) / 10,
                    fuso: [z, hy]
                }
            default:
                return {
                    lng: lng, 
                    lat: lat
                }
        }
    }

    /**
     * Converte as coordenadas de qualquer notação e datum para latlong.
     * @param {Object} coordinates - Objeto contendo as coordenadas.
     * @param {String} notation - String que indica a notação a ser convertida.
     * @param {String} datum - String contendo o nome do datum a ser convertido.
     */
    toLngLat = function(coordinates, notation, datum) {
        let projection = ' +proj=longlat'
        let x, y
        switch (notation) {
            case 'd':
                y = this._tolerantParseFloat(coordinates.lat[0])
                x = this._tolerantParseFloat(coordinates.lng[0])
                break
            case 'dm':
                y = this._tolerantParseFloat(coordinates.lat[0]) +
                    this._tolerantParseFloat(coordinates.lat[1]) / 60.0
                x = this._tolerantParseFloat(coordinates.lng[0]) +
                    this._tolerantParseFloat(coordinates.lng[1]) / 60.0
                break
            case 'dms':
                y = this._tolerantParseFloat(coordinates.lat[0]) +
                    this._tolerantParseFloat(coordinates.lat[1]) / 60.0 +
                    this._tolerantParseFloat(coordinates.lat[2]) / 3600.0
                x = this._tolerantParseFloat(coordinates.lng[0]) +
                    this._tolerantParseFloat(coordinates.lng[1]) / 60.0 +
                    this._tolerantParseFloat(coordinates.lng[2]) / 3600.0
                break
            case 'utm':
                y = this._tolerantParseFloat(coordinates.lat[0])
                x = this._tolerantParseFloat(coordinates.lng[0])
                projection = ' +proj=utm +zone=' + this._tolerantParseInt(coordinates.fuso[0])
                if (coordinates.fuso[1] === 'S') {
                    projection += ' +south'
                } else {
                    projection += ' +north'
                }
                break
            default:
                throw new Error('Notação não suportada.')
        }
        if (coordinates.lat[coordinates.lat.length - 1] === 'S') {
            y = -y
        }
        if (coordinates.lng[coordinates.lng.length - 1] === 'W') {
            x = -x
        }
        let projStr = datumProjection[datum] + projection
        return proj4(projStr, 'EPSG:4326', [x, y])
    }
})();